# Introduction
This is a technical implementation of my King In The Middle attack that can be found [here](https://blog.haywirehax.com/writeups/2019/king-in-the-middle-attack).
![Setup of the King In The Middle](layout.png)
This is a concept of mine that expands the usage of a man in the middle. Most of the time you end up setting up all these features at one point in time so it would be hugely beneficial to automate them.

## Phishing
> sites that enable HSTS won't be able to be spoofed.

## Usage
1. Run `install.sh`
1. Change settings in `playbook.yml`
2. Place the files that need to go into the `/etc` folder in the `./etc` folder
3. Place your phishing sites in the `.\web`
4. Place the nginx conf in the `./nginx` folder
2. Add hosts to hosts.ini
3. An example setup is placed into the exampleconf folder
3. run `start.sh`

## Working features
- Adds mail user
- Sets up **logging**
- Sets up the **phishing sites**
- Sets up nginx
- Installs php
- Installs dovecot and postfix (doesn't have any working config yet)
